{{/*
Expand the name of the chart.
*/}}
{{- define "barbatos.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "barbatos.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "barbatos.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "barbatos.labels" -}}
helm.sh/chart: {{ include "barbatos.chart" . }}
{{ include "barbatos.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "barbatos.selectorLabels" -}}
app.kubernetes.io/name: {{ include "barbatos.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "barbatos.gbk.image" -}}
image: '{{- default "registry.gitlab.com/echoii/ci/kteam-tools/gbk" .Values.gbkImageRepo }}{{- if .Values.gbkImageTag }}:{{ .Values.gbkImageTag }}{{- end }}'
{{- end }}

{{- define "barbatos.gbk.servicePort" -}}
{{- default 31245 .Values.gbkServicePort }}
{{- end }}

{{- define "barbatos.gbk.init-git-ref-linux.image" -}}
image: '{{- default "registry.gitlab.com/echoii/ci/barbatos/production" .Values.gbkInitGitRefLinuxImageRepo }}:{{- default "git-ref" .Values.gbkInitGitRefLinuxImageTag }}'
{{- end }}

{{- define "barbatos.gbk.init-git-ref-linux.args" -}}
'--break={{ default "fetch" .Values.gbkInitGitRefLinuxBreak }}'
{{- end }}

{{- define "barbatos.chroot.variants" -}}
{{- if .Values.chrootVariants }}
{{- range $val := .Values.chrootVariants }}
{{ $val.series }}-{{ $val.arch }}
{{- end }}
{{- else }}
trusty-amd64
trusty-i386
xenial-amd64
xenial-i386
bionic-amd64
focal-amd64
groovy-amd64
{{- end }}
{{- end }}
