#!/bin/sh

set -e

GIT_DIR=/srv/git/reference.git
GIT_REMOTES_DEFAULT="
  korg:https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  precise:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/precise
  trusty:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/trusty
  xenial:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/xenial
  bionic:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/bionic
  cosmic:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/cosmic
  disco:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/disco
  eoan:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/eoan
  focal:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/focal
  groovy:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/groovy
  unstable:https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/unstable
  oem-bionic:https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-oem/+git/bionic
  oem-focal:https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-oem/+git/focal
  oem-osp1-bionic:https://git.launchpad.net/~canonical-kernel/ubuntu/+source/linux-oem-osp1/+git/bionic
"

# --break=<init|fetch>
opt_break=
# --resync-period=<sleep period>
opt_resync_period=3600

while [ $# -gt 0 ]; do
  case "$1" in
  "--break"|--break=*)
    if [ -n "${1#*=}" ] && [ "${1#*=}" != "$1" ]; then
      opt_break=${1#*=}
    elif [ $# -ge 2 ]; then
      opt_break=$2; shift
    fi
    ;;
  "--verbose")
    set -x; ;;
  "--resync-period"|--resync-period=*)
    if [ -n "${1#*=}" ] && [ "${1#*=}" != "$1" ]; then
      opt_resync_period=${1#*=}
    elif [ $# -ge 2 ]; then
      opt_resync_period=$2; shift
    fi
    ;;
  "--help")
    echo "Usage: $0 [--verbose] [--break=<init|fetch>] [--resync-period=<seconds>]"; exit 0 ;;
  *)
    echo "Unknown argument '$1'. Exit." >&2; exit 1 ;;
  esac
  shift
done

GIT_REMOTES=${GIT_REMOTES:-${GIT_REMOTES_DEFAULT}}

if [ ! -f "${GIT_DIR}/config" ]; then
  mkdir -p "${GIT_DIR}"
  cd "${GIT_DIR}"
  git init --bare
  for remote in ${GIT_REMOTES}; do
    git remote add "${remote%%:*}" "${remote#*:}"
  done

fi

[ "${opt_break}" != "init" ] || exit 0;

cd "${GIT_DIR}"
while true; do
  git fetch -p --all
  [ "${opt_break}" != "fetch" ] || exit 0;

  sleep "${opt_resync_period}"
done
